## Docker for Mac's 42 Born2code school

#### Install boot2docker with brew
	1_install_boot2docker.md

#### Sharing localfile with docker (optional)
	2_sharing_file_2_boot2docker_2_docker.md

#### replace 192.168.xxx.yyy with localhost (optional)
	3_opt_replace_ip_2_localhost.sh